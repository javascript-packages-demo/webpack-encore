# [webpack-encore](https://www.npmjs.com/package/@symfony/webpack-encore)

A simple but powerful API for processing & compiling assets built around Webpack

## Official documentation
* [*Managing CSS and JavaScript*](https://symfony.com/doc/current/frontend.html)

## Unofficial documentation
* [*Bulma and Symfony’s Webpack Encore*
  ](https://medium.com/weekly-webtips/bulma-and-symfonys-webpack-encore-f79f48c328aa)
  2020-09 Amir Bizimana
* [*Getting started With React in Symfony Using Webpack Encore*
  ](https://www.cloudways.com/blog/symfony-react-using-webpack-encore/)
* [*Getting Started with Vue.js in Symfony*
  ](https://www.cloudways.com/blog/symfony-vuejs-app/)